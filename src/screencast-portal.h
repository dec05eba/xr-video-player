/* screencast-portal.h
 *
 * Copyright 2022 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <wayland-client.h>
#include <stdbool.h>


typedef struct {
	bool visible;
	bool valid;
	int x, y;
	int hotspot_x, hotspot_y;
	int width, height;
	GLuint texture;
} screencast_portal_cursor;

void screencast_portal_load(void);
void screencast_portal_capture_show(void *data);
void screencast_portal_capture_hide(void *data);
uint32_t screencast_portal_capture_get_width(void *data);
uint32_t screencast_portal_capture_get_height(void *data);
GLuint screencast_portal_capture_get_texture(void *data);
screencast_portal_cursor *screencast_portal_capture_get_cursor_data(void *data);

void *screencast_portal_capture_create(struct gl_egl_data *egl_data);
void screencast_portal_capture_destroy(void *data);

#ifdef __cplusplus
}
#endif
