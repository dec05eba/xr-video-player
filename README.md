This is a fork of https://git.dec05eba.com/vr-video-player.

A virtual reality video player for Linux running wayland, based on Monados's openxr example project: [openxr-simple-example](https://gitlab.freedesktop.org/monado/demos/openxr-simple-example)

It was tested against the current git version of [Monado's](https://gitlab.freedesktop.org/monado/monado) wmr driver.

Currently only works with stereo video and equirectangular cube maps (youtube 360 videos) when used for vr videos, but if the xr-video-player is launched with the `--plane` option then you can view
the video as a regular video in vr without depth (like a cinema).

# Building

Currently the recommend way to build this application is via cmake.

Dependencies needed when building from source: `git cmake glm glew glib2 sdl2 openxr wayland pipewire`.

The following commands should build the application successfully.

```bash
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Debug
cmake --build .
```

# How to use

xr-video-player has two options. Either capture a window and view it in vr (works only on wayland) or a work-in-progress built-in mpv option.

## Using the built-in video player

To play a video with the built-in mpv player, run xr-video-player like so:
```
./xr-video-player --sphere --video <file-path>
```

for example:
```
./xr-video-player --sphere --video /home/adam/Videos/my-cool-vr-video.mp4
```
`--sphere` can be replaced with `--flat`, `plane` or `--sphere360` for different display modes.

Currently only the following key bindings exist:
```
Left Arrow: Skip -5.0 seconds
Right Arrow: Skip +5.0 seconds
Space: Pause video
```

## Capturing a window
Most likely needed to get the window/desktop popup window: install `xdg-desktop-portal`.

Launch a video in a video player (I recommend mpv, because browsers, smplayer and vlc player remove the vr for 360 videos) and resize it to fit your monitor or larger for best quality and then,

if you want to watch 180 degree stereoscopic videos then run:
```
./xr-video-player
```
and select via the "Screen Sharing - Portal" popup window your application.

if you want to watch side-by-side stereoscopic videos or games (flat) then run:
```
./xr-video-player --flat
```
and select your application.

if the side-by-side video is mirrored so that the left side is on the right side of the video, then run:
```
./xr-video-player --flat --right-left
```
and select your application.

if you want to watch a regular non-stereoscopic video, then run:
```
./xr-video-player --plane
```
and select your application.

if you want to watch a 360 video (for example a youtube video), then run:
```
./xr-video-player --sphere360
```
and select your application.

# Input options

There currently do not exist any global shortcuts, every shortcut has to be used while the window is focused.

The video might not be in front of you, so to move the video in front of you, you can do any of the following:
* Press the "W" key while the xr-video-player is focused
* Send a SIGUSR1 signal to the application, using the following command: `killall -USR1 xr-video-player`

You can zoom the view with Q/E.

You can freeze the current view by pressing F.

You can launch xr-video-player without any arguments to show a list of all arguments.

## Not tested for openxr version:

Note: If the cursor position is weird and does not match what you are seeing in stereoscopic vr mode, then try running the vr video player with the --cursor-wrap option:

--cursor-wrap and --no-cursor-wrap changes the behavior of the cursor in steroscopic mode. Usually in games the game view is mirrored but the cursor is not and the center of the
game which is normally at the center moves to 1/4 and 3/4 of the window. With --cursor-wrap, the cursor position in VR will match the real position of the
cursor relative to the window and with --no-cursor-wrap the cursor will match the position of the cursor as the game sees it.

Note: --cursor-scale is set to 0 by default in 180 degrees stereoscopic mode and 2 in other modes. Also --no-cursor-wrap is set by default in --flat mode.\
Note: When fullscreening a video, the video player can add black bars to the top and bottom. If you do not want black blacks then you either have to view the video in windowed mode or try launching mpv with the `--panscan=1.0` option.

# Games

This vr video player can also be used to play games in VR to to get a 3D effect, and even for games that don't support VR.\
For games such as Trine that have built-in side-by-side view, you can launch it with proton and there is a launch option for side-by-side view. Select this and when the game launches, get the X11 window id of the game
and launch vr video player with the `--flat` option.\
For games that do not have built-in side-by-side view, you can use [ReShade](https://reshade.me/) (or [vkBasalt](https://github.com/DadSchoorse/vkBasalt) for linux native games) and [SuperDepth3D_VR.fx](https://github.com/BlueSkyDefender/Depth3D) effect with proton. This will make the game render with side-by-side view and you can then launch vr video player with the `--flat` option and chose the window of the game. The game you are playing might require settings to be changed manually in ReShade for SuperDepth3D_VR to make it look better.

# Known Issues

On KDE if you change the window size of your chosen application it does not change the metadata and size of the data that gets transmitted to xr-video-player via pipewire. This should be fixed with the release of KWin (v5.26.5) because of this [PR](https://invent.kde.org/plasma/kwin/-/merge_requests/3264). Most notably this is if your window changes its aspect-ratio dramatically.

# Contributions and new issues

Feel free to open a pull request if you have something to add.

Open an issue if you find a problem or have a suggestion.
Keep in mind that any suggestion may or may not be implemented in any reasonable time frame, so if you really want your suggestion to happen, my advise is to do it yourself and open a pull request.

# Credits

Huge parts taken from [openxr-simple-example](https://gitlab.freedesktop.org/monado/demos/openxr-simple-example) and adapted.

The pipewire implementation by [OBS](https://github.com/obsproject/obs-studio/tree/master/plugins/linux-pipewire)

[vr-video-player by dec05eba](https://git.dec05eba.com/vr-video-player/about/)

Original base for main.cpp [hellovr_opengl](https://github.com/ValveSoftware/openvr/tree/master/samples)

