cmake_minimum_required(VERSION 3.5.1)
project(xr-video-player)

if (POLICY CMP0072)
  cmake_policy (SET CMP0072 NEW)
endif(POLICY CMP0072)

list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake")

INCLUDE(FindPkgConfig)
PKG_SEARCH_MODULE(SDL2 REQUIRED sdl2)
find_package(OpenGL REQUIRED COMPONENTS OpenGL EGL)
PKG_SEARCH_MODULE(WAYLAND_CLIENT REQUIRED wayland-client)
PKG_SEARCH_MODULE(GLEW REQUIRED glew)
PKG_SEARCH_MODULE(GIO gio-2.0 REQUIRED gio)
PKG_SEARCH_MODULE(GIO_UNIX gio-unix-2.0 REQUIRED gio_unix)
PKG_SEARCH_MODULE(GLIB glib-2.0 REQUIRED glib)
PKG_SEARCH_MODULE(PIPEWIRE libpipewire-0.3 REQUIRED pipewire)
PKG_SEARCH_MODULE(SPA libspa-0.2 REQUIRED spa)
PKG_SEARCH_MODULE(MPV REQUIRED mpv)

find_package(glm REQUIRED)

include_directories(
  ./src
  ${OPENGL_INCLUDE_DIRS}
  ${OPENGL_EGL_INCLUDE_DIRS}
  ${GIO_INCLUDE_DIRS}
  ${GIO_UNIX_INCLUDE_DIRS}
  ${GLIB_INCLUDE_DIRS}
  ${PIPEWIRE_INCLUDE_DIRS}
  ${SPA_INCLUDE_DIRS}
  ${GLEW_INCLUDE_DIRS}
  ${SDL2_INCLUDE_DIRS}
  ${WAYLAND_CLIENT_INCLUDE_DIRS}
  ${MPV_INCLUDE_DIRS}
)

add_executable(xr-video-player
  src/gl-egl-common.c
  src/pipewire.c
  src/portal.c
  src/screencast-portal.c
  src/mpv.cpp
  src/main.cpp
)

install(TARGETS xr-video-player)
target_link_libraries(
  xr-video-player
  ${GLIB_LIBRARIES}
  ${GIO_LIBRARIES}
  ${GIO_UNIX_LIBRARIES}
  ${WAYLAND_CLIENT_LIBRARIES}
  ${GLEW_LIBRARIES}
  ${X11_LIBRARIES}
  ${OPENGL_LIBRARIES}
  OpenGL::EGL
  ${PIPEWIRE_LIBRARIES}
  ${SDL2_LIBRARIES}
  ${MPV_LIBRARIES}
)

# First try openxr.pc from OpenXR SDK
pkg_search_module(OPENXR openxr)
if (OPENXR_FOUND)
  MESSAGE("OpenXR found with pkg-config")
  target_link_libraries(xr-video-player ${OPENXR_LIBRARIES})

# Second, try OpenXRConfig.cmake from OpenXR SDK
else()
  MESSAGE("OpenXR not found with pkg-config, trying cmake script")

  # current issue in upstream OpenXR cmake files requires us to find Threads on our own
  find_package(Threads REQUIRED)

  find_package(OpenXR REQUIRED)
  if (NOT OpenXR_FOUND)
     MESSAGE(FATAL_ERROR "OpenXR not found!")
  endif()

  target_include_directories(xr-video-player ${OpenXR_INCLUDE_DIR})
  target_link_libraries(xr-video-player OpenXR::openxr_loader)
endif()
